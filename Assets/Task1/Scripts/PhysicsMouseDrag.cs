using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Rigidbody2D))]
public class PhysicsMouseDrag : MonoBehaviour
{
    [SerializeField] float dragForce = 5.0f;
    Rigidbody2D _rb;
    Camera _camera;
    Transform _transform;

    bool _atDrag = false;


    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _camera = Camera.main;
        Debug.Assert(_camera);
        _transform = transform;
    }

    private void OnMouseDrag()
    {
        _atDrag = true;
    }

    private void FixedUpdate()
    {
        if(_atDrag)
        {
            Vector3 mousePositionInWorldSpace = _camera.ScreenToWorldPoint(Input.mousePosition);
            Vector3 toMousePosition = (_transform.position - mousePositionInWorldSpace);
            
            // so that we will not overshoot
            toMousePosition /= toMousePosition.sqrMagnitude;

            _rb.AddForce(toMousePosition * dragForce);

            _atDrag = false;
        }
    }
}
