﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class TowerCountDetector : MonoBehaviour
{
    [SerializeField] int poolId;
    [SerializeField] int expectedTowerCount;
    [SerializeField] UnityEvent onCountReached;

    bool _alreadyCalled;

    private void Start()
    {
        TowerManager.Instance.onSpawn.AddListener(Check);
    }

    void OnEnable()
    {
        Restart();
    }

    void OnSpawn()
    {
        Restart();
    }

    public void Restart()
    {
        _alreadyCalled = false;
    }

    public void Check()
    {
        if(!_alreadyCalled &&  TowerManager.Instance.GetSpawnedCount(poolId) >= expectedTowerCount)
        {
            onCountReached.Invoke();
            _alreadyCalled = true;
        }
    }
}
