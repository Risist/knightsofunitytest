﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class BulletController : MonoBehaviour
{
    [SerializeField] RangedFloat traversalDistanceRange;
    [SerializeField] float movementSpeed;
    [Space]
    [SerializeField] float distanceTollerance;
    [SerializeField] UnityEvent onTraversalFinished;

    MinimalTimer tMovement = new MinimalTimer();
    Vector3 _targetPosition;
    float _traversalDistance;

    bool _alreadyInvokedEvent = false;

    void OnSpawn()
    {
        _traversalDistance = traversalDistanceRange.GetRandom();
        _targetPosition = transform.position + transform.up * _traversalDistance;
        tMovement.Restart();
        _alreadyInvokedEvent = false;
    }

    private void FixedUpdate()
    {
        // update position
        Vector3 currentPosition = transform.position;
        Vector3 newPosition = Vector3.Lerp(currentPosition, _targetPosition, tMovement.ElapsedTime() / _traversalDistance * movementSpeed);

        transform.position = newPosition;

        // UpdateEvent
        if (!_alreadyInvokedEvent && (newPosition - _targetPosition).sqrMagnitude < distanceTollerance.Sq())
        {
            onTraversalFinished.Invoke();
        }
    }
}
