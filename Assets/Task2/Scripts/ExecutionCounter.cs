﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class ExecutionCounter : MonoBehaviour
{
    [SerializeField] int maxActivations;
    [SerializeField] UnityEvent action;
    [SerializeField] UnityEvent onDeactivate;

    int _activations = 0;
    bool _alreadyDeactivated = false;

    void OnEnable()
    {
        Restart();
    }

    void OnSpawn()
    {
        Restart();
    }

    public void Restart()
    {
        _activations = 0;
        _alreadyDeactivated = false;
    }

    public void Call()
    {
        if (_activations >= maxActivations)
        {
            if(!_alreadyDeactivated)
            {
                onDeactivate.Invoke();
                _alreadyDeactivated = true;
            }

            return;
        }

        ++_activations;
        action.Invoke();
    }
}
