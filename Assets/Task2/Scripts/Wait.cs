﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class Wait : MonoBehaviour
{
    [SerializeField] Timer timer;
    [SerializeField] UnityEvent action;

    bool _alreadyCalled;

    void OnEnable()
    {
        Restart();
    }
    void OnSpawn()
    {
        Restart();
    }

    public void Restart()
    {
        _alreadyCalled = false;
        timer.Restart();
    }


    private void Update()
    {
        if(!_alreadyCalled && timer.IsReady())
        {
            action.Invoke();
            _alreadyCalled = true;
        }
    }
}
