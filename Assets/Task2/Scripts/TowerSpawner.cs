﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class TowerSpawner : MonoBehaviour
{
    [SerializeField] int poolId;
    [SerializeField] int maxSpawnedObjects;

    public void Spawn()
    {
        if(TowerManager.Instance.GetSpawnedCount(poolId) >= maxSpawnedObjects)
        {
            return;
        }

        var towerController = TowerManager.Instance.SpawnTower(transform.position, transform.eulerAngles.z, poolId);
    }
}
