﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class BulletSpawner : MonoBehaviour
{
    [SerializeField] int poolId;

    public void Spawn()
    {
        var bulletcontroller = BulletManager.Instance.SpawnBullet(transform.position, transform.eulerAngles.z, poolId);
    }

}
