﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class BulletManager : MonoSingleton<BulletManager>
{
    [SerializeField] ObjectPool[] objectPools;
    [SerializeField] UnityEvent onSpawn;

    public BulletController SpawnBullet(Vector2 spawnLocation, float spawnRotation, int poolId)
    {
        BulletController bulletController = null;
        
        objectPools[poolId].SpawnObject((obj) =>
        {
            obj.transform.position = spawnLocation;
            obj.transform.eulerAngles = new Vector3(0, 0, spawnRotation);

            bulletController = obj.GetComponent<BulletController>();

            Debug.Assert(bulletController);
        });

        onSpawn.Invoke();
        return bulletController;
    }
}
