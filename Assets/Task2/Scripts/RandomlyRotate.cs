using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RandomlyRotate : MonoBehaviour
{
    [SerializeField] Transform rotatingTransform;
    [SerializeField] Timer tRotation;
    [SerializeField] RangedFloat rotationRange;
    [SerializeField, Range(0,1)] float rotationSpeed;
    [Space]
    [SerializeField] float rotationTolerance;
    [SerializeField] UnityEvent onRotationFinish;

    float _targetRotation;
    bool _eventActivated = true;

    void Start()
    {
        if (!rotatingTransform)
            rotatingTransform = transform;

        _targetRotation = rotatingTransform.eulerAngles.z;
    }

    void UpdateRotation()
    {
        float currentRotation = rotatingTransform.eulerAngles.z;
        float newRotation = Mathf.LerpAngle(currentRotation, _targetRotation, rotationSpeed);
        rotatingTransform.eulerAngles = new Vector3(0, 0, newRotation);
    }

    void UpdateTargetRotation()
    {
        if(tRotation.IsReadyRestart())
        {
            _targetRotation += rotationRange.GetRandomSigned();
            _eventActivated = false;
        }
    }

    void UpdateEvent()
    {
        if (_eventActivated)
            return;

        float currentRotation = rotatingTransform.eulerAngles.z;
        if(Mathf.DeltaAngle(currentRotation, _targetRotation).Sq() < rotationTolerance.Sq()  )
        {
            onRotationFinish.Invoke();
            _eventActivated = true;
        }
    }

    void FixedUpdate()
    {
        UpdateTargetRotation();
        UpdateRotation();
        UpdateEvent();
    }
}
