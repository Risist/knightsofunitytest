﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class SwitchColor : MonoBehaviour
{
    [SerializeField] SpriteRenderer[] spriteRenderers;
    [SerializeField] Color[] colorOptions;

    public void Switch(int id)
    {
        foreach(var it in spriteRenderers)
        {
            it.color = colorOptions[id];
        }
    }

    
}
