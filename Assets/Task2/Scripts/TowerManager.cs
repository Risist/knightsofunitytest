﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class TowerManager : MonoSingleton<TowerManager>
{
    [SerializeField] ObjectPool[] objectPools;
    public UnityEvent onSpawn;

    public int GetSpawnedCount(int poolId)
    {
        return objectPools[poolId].SpawnedObjectsCount;
    }

    public TowerController SpawnTower(Vector2 spawnLocation, float spawnRotation, int poolId)
    {
        TowerController towerController = null;

        objectPools[poolId].SpawnObject((obj) =>
        {
            obj.transform.position = spawnLocation;
            obj.transform.eulerAngles = new Vector3(0, 0, spawnRotation);

            towerController = obj.GetComponent<TowerController>();

            Debug.Assert(towerController);
        });

        onSpawn.Invoke();
        return towerController;

    }
}

