﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UITowerCountDisplayer : MonoBehaviour
{
    [SerializeField] int towerPoolId;
    Text _text;

    void Awake()
    {
        _text = GetComponent<Text>();
        UpdateText();
    }

    public void UpdateText()
    {
        _text.text = "Towes: " + (TowerManager.Instance.GetSpawnedCount(towerPoolId) + 1);
    }
}
