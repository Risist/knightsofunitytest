﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class SpawnDetector : MonoBehaviour
{
    [SerializeField] UnityEvent onSpawn;
    [SerializeField] UnityEvent onDespawn;

    void OnSpawn()
    {
        onSpawn.Invoke();
    }

    void OnDespawn()
    {
        onDespawn.Invoke();
    }
}
